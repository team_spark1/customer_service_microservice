package com.spark.customer_service_microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerServiceMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceMicroserviceApplication.class, args);
	}

}
